QT += qml quick

CONFIG += c++11

SOURCES += \
    gomodule.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include($$PWD/../deployment.pri)

HEADERS += \
    gomodule.h
