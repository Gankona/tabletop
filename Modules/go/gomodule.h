#ifndef GOMODULE_H
#define GOMODULE_H

#include <QDebug>
#include "../../Client/f_basicplugin.h"

class GoModule : public FBasicPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.gankona.TableTop.FBasicPlugin")
    Q_INTERFACES(FBasicPlugin)
public:
    GoModule();
    QUrl getMainQmlUrl(QString mode) Q_DECL_OVERRIDE;
    void closeApp()  Q_DECL_OVERRIDE;
    void setAccountName(QString login)  Q_DECL_OVERRIDE;
    bool isLocalPresent()  Q_DECL_OVERRIDE;
    bool isNetworkPresent()  Q_DECL_OVERRIDE;
    bool isOnePlayerPresent()  Q_DECL_OVERRIDE;
    bool isDualPlayerPresent() Q_DECL_OVERRIDE;
    QString getName() Q_DECL_OVERRIDE;

public slots:
    QString resu();

signals:
    void signalBackToMenu()  Q_DECL_OVERRIDE;
    void signalCloseApp()  Q_DECL_OVERRIDE;
    QString signa();
};

#endif // GOMODULE_H
