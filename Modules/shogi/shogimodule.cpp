#include "shogimodule.h"

ShogiModule::ShogiModule()
{
    qDebug() << "start";
    QObject::connect(this, SIGNAL(signa()), this, SLOT(resu()));
    qDebug() << signa();
}

QUrl ShogiModule::getMainQmlUrl(QString mode)
{
    return QUrl("qrc:/shogi/qml/shogi.qml");
}

QString ShogiModule::getName()
{
    return "shogi";
}

void ShogiModule::closeApp(){}

void ShogiModule::setAccountName(QString login){}

bool ShogiModule::isLocalPresent()
{
    return false;
}

bool ShogiModule::isNetworkPresent()
{
    return false;
}

bool ShogiModule::isOnePlayerPresent()
{
    return true;
}

bool ShogiModule::isDualPlayerPresent()
{
    return false;
}

QString ShogiModule::resu()
{
    qDebug() << "resu";
    return "qwe";
}
