#-------------------------------------------------
#
# Project created by QtCreator 2016-06-22T08:52:59
#
#-------------------------------------------------

QT       += core gui

TARGET = shogi

include($$PWD/../deployment.pri)

SOURCES += shogimodule.cpp

HEADERS += shogimodule.h

OTHER_FILES += $$PWD/qml/*qml

DISTFILES += \
    shogi.qml

RESOURCES += \
    qml.qrc
