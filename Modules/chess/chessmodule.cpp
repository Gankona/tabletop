#include "chessmodule.h"

ChessModule::ChessModule()
{
    qDebug() << "start";
    QObject::connect(this, SIGNAL(signa()), this, SLOT(resu()));
    qDebug() << signa();
}

QUrl ChessModule::getMainQmlUrl(QString mode)
{
    return QUrl("qrc:/shogi.qml");
}

QString ChessModule::getName()
{
    return "Chess";
}

void ChessModule::closeApp(){}

void ChessModule::setAccountName(QString login){}

bool ChessModule::isLocalPresent()
{
    return false;
}

bool ChessModule::isNetworkPresent()
{
    return false;
}

bool ChessModule::isOnePlayerPresent()
{
    return false;
}

bool ChessModule::isDualPlayerPresent()
{
    return false;
}

QString ChessModule::resu()
{
    qDebug() << "resu";
    return "qwe";
}
