#ifndef CHESSMODULE_H
#define CHESSMODULE_H

#include <QDebug>
#include "../../Client/f_basicplugin.h"

class ChessModule : public FBasicPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.gankona.TableTop.FBasicPlugin")
    Q_INTERFACES(FBasicPlugin)
public:
    ChessModule();
    QUrl getMainQmlUrl(QString mode) Q_DECL_OVERRIDE;
    void closeApp()  Q_DECL_OVERRIDE;
    void setAccountName(QString login)  Q_DECL_OVERRIDE;
    bool isLocalPresent()  Q_DECL_OVERRIDE;
    bool isNetworkPresent()  Q_DECL_OVERRIDE;
    bool isOnePlayerPresent()  Q_DECL_OVERRIDE;
    bool isDualPlayerPresent() Q_DECL_OVERRIDE;
    QString getName() Q_DECL_OVERRIDE;

public slots:
    QString resu();

signals:
    void signalBackToMenu()  Q_DECL_OVERRIDE;
    void signalCloseApp()  Q_DECL_OVERRIDE;
    QString signa();
};

#endif // CHESSMODULE_H
