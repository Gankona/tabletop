import QtQuick 2.7
import QtQuick.Controls 2.0
import "qrc:/xiangqi/qml"

Item {
    anchors.fill: parent

    Rectangle {
        id: menuDesk
        height: (parent.height-playDesk.height)/3
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        //color: "navy"
    }

    Rectangle {
        id: topDesk
        height: (parent.height-playDesk.height)/3
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: menuDesk.bottom
        //color: "green"
    }

    Desk {
        id: playDesk
        height: parent.width/10*11
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: topDesk.bottom
    }

    Rectangle {
        id: bottomDesk
        height: (parent.height-playDesk.height)/3
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: playDesk.bottom
        //color: "orange"
    }
}
