import QtQuick 2.5

Rectangle {
    color: "#bf4c19"

    //property int sizeSquard: value
    property int widthSquare: 36//height/11
    property int shiftH: (height - widthSquare*9)/2
    property int shiftW: (width - widthSquare*8)/2

    Rectangle {
        id: orangeRect
        height: widthSquare*9
        width: widthSquare*8
        x: shiftW
        y: shiftH
        color: "#ffcc00"

        /*
        Rectangle {
            anchors.left: parent.left
            anchors.right: parent.right
            height: widthSquare
            y: shiftH + 3*widthSquare
            color: "navy"
        }*/
    }

    Canvas {
        anchors.fill: parent

        onPaint: {
            var context = getContext("2d")
            context.beginPath();
            context.lineWidth = 2;
            context.strokeStyle = "black"

            //border
            context.moveTo(shiftW, shiftH);
            context.lineTo(shiftW + 8*widthSquare, shiftH);

            context.moveTo(shiftW + 8*widthSquare, shiftH);
            context.lineTo(shiftW + 8*widthSquare, shiftH + 9*widthSquare);

            context.moveTo(shiftW + 8*widthSquare, shiftH + 9*widthSquare);
            context.lineTo(shiftW, shiftH + 9*widthSquare);

            context.moveTo(shiftW, shiftH + 9*widthSquare);
            context.lineTo(shiftW, shiftH);

            //horizontal lines
            context.moveTo(shiftW, shiftH + widthSquare);
            context.lineTo(shiftW + 8*widthSquare, shiftH + widthSquare);
            context.moveTo(shiftW, shiftH + 2*widthSquare);
            context.lineTo(shiftW + 8*widthSquare, shiftH + 2*widthSquare);
            context.moveTo(shiftW, shiftH + 3*widthSquare);
            context.lineTo(shiftW + 8*widthSquare, shiftH + 3*widthSquare);
            context.moveTo(shiftW, shiftH + 4*widthSquare);
            context.lineTo(shiftW + 8*widthSquare, shiftH + 4*widthSquare);
            context.moveTo(shiftW, shiftH + 5*widthSquare);
            context.lineTo(shiftW + 8*widthSquare, shiftH + 5*widthSquare);
            context.moveTo(shiftW, shiftH + 6*widthSquare);
            context.lineTo(shiftW + 8*widthSquare, shiftH + 6*widthSquare);
            context.moveTo(shiftW, shiftH + 7*widthSquare);
            context.lineTo(shiftW + 8*widthSquare, shiftH + 7*widthSquare);
            context.moveTo(shiftW, shiftH + 8*widthSquare);
            context.lineTo(shiftW + 8*widthSquare, shiftH + 8*widthSquare);

            //up desk lines
            context.moveTo(shiftW + widthSquare, shiftH);
            context.lineTo(shiftW + widthSquare, shiftH + 4*widthSquare);
            context.moveTo(shiftW + 2*widthSquare, shiftH);
            context.lineTo(shiftW + 2*widthSquare, shiftH + 4*widthSquare);
            context.moveTo(shiftW + 3*widthSquare, shiftH);
            context.lineTo(shiftW + 3*widthSquare, shiftH + 4*widthSquare);
            context.moveTo(shiftW + 4*widthSquare, shiftH);
            context.lineTo(shiftW + 4*widthSquare, shiftH + 4*widthSquare);
            context.moveTo(shiftW + 5*widthSquare, shiftH);
            context.lineTo(shiftW + 5*widthSquare, shiftH + 4*widthSquare);
            context.moveTo(shiftW + 6*widthSquare, shiftH);
            context.lineTo(shiftW + 6*widthSquare, shiftH + 4*widthSquare);
            context.moveTo(shiftW + 7*widthSquare, shiftH);
            context.lineTo(shiftW + 7*widthSquare, shiftH + 4*widthSquare);

            //down desk lines
            context.moveTo(shiftW + widthSquare, shiftH + 9*widthSquare);
            context.lineTo(shiftW + widthSquare, shiftH + 5*widthSquare);
            context.moveTo(shiftW + 2*widthSquare, shiftH + 9*widthSquare);
            context.lineTo(shiftW + 2*widthSquare, shiftH + 5*widthSquare);
            context.moveTo(shiftW + 3*widthSquare, shiftH + 9*widthSquare);
            context.lineTo(shiftW + 3*widthSquare, shiftH + 5*widthSquare);
            context.moveTo(shiftW + 4*widthSquare, shiftH + 9*widthSquare);
            context.lineTo(shiftW + 4*widthSquare, shiftH + 5*widthSquare);
            context.moveTo(shiftW + 5*widthSquare, shiftH + 9*widthSquare);
            context.lineTo(shiftW + 5*widthSquare, shiftH + 5*widthSquare);
            context.moveTo(shiftW + 6*widthSquare, shiftH + 9*widthSquare);
            context.lineTo(shiftW + 6*widthSquare, shiftH + 5*widthSquare);
            context.moveTo(shiftW + 7*widthSquare, shiftH + 9*widthSquare);
            context.lineTo(shiftW + 7*widthSquare, shiftH + 5*widthSquare);

            //draw
            context.stroke();

            context.lineWidth = 1.5;
            //up castle
            context.moveTo(shiftW + 3*widthSquare, shiftH);
            context.lineTo(shiftW + 5*widthSquare, shiftH + 2*widthSquare);
            context.moveTo(shiftW + 3*widthSquare, shiftH + 2*widthSquare);
            context.lineTo(shiftW + 5*widthSquare, shiftH);

            //down castle
            context.moveTo(shiftW + 3*widthSquare, shiftH + 7*widthSquare);
            context.lineTo(shiftW + 5*widthSquare, shiftH + 9*widthSquare);
            context.moveTo(shiftW + 3*widthSquare, shiftH + 9*widthSquare);
            context.lineTo(shiftW + 5*widthSquare, shiftH + 7*widthSquare);

            //draw
            context.stroke();
        }
    }

    //figure
    Image {
        id: redGeneral
        source: "qrc:/xiangqi/files/svg/generalR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 4 * widthSquare - 0.45 * widthSquare
        y: shiftH - 0.45 * widthSquare
    }
    Image {
        id: redAdvisor1
        source: "qrc:/xiangqi/files/svg/advisorR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 3 * widthSquare - 0.45 * widthSquare
        y: shiftH - 0.45 * widthSquare
    }
    Image {
        id: redAdvisor2
        source: "qrc:/xiangqi/files/svg/advisorR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 5 * widthSquare - 0.45 * widthSquare
        y: shiftH - 0.45 * widthSquare
    }
    Image {
        id: redCannon1
        source: "qrc:/xiangqi/files/svg/cannonR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + widthSquare - 0.45 * widthSquare
        y: shiftH + 2 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: redCannon2
        source: "qrc:/xiangqi/files/svg/cannonR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 7 * widthSquare - 0.45 * widthSquare
        y: shiftH + 2 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: redChariot1
        source: "qrc:/xiangqi/files/svg/chariotR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW - 0.45 * widthSquare
        y: shiftH - 0.45 * widthSquare
    }
    Image {
        id: redChariot2
        source: "qrc:/xiangqi/files/svg/chariotR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 8 * widthSquare - 0.45 * widthSquare
        y: shiftH - 0.45 * widthSquare
    }
    Image {
        id: redElephant1
        source: "qrc:/xiangqi/files/svg/elephantR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 2 * widthSquare - 0.45 * widthSquare
        y: shiftH - 0.45 * widthSquare
    }
    Image {
        id: redElephant2
        source: "qrc:/xiangqi/files/svg/elephantR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 6 * widthSquare - 0.45 * widthSquare
        y: shiftH - 0.45 * widthSquare
    }
    Image {
        id: redHorse1
        source: "qrc:/xiangqi/files/svg/horseR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + widthSquare - 0.45 * widthSquare
        y: shiftH - 0.45 * widthSquare
    }
    Image {
        id: redHorse2
        source: "qrc:/xiangqi/files/svg/horseR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 7 * widthSquare - 0.45 * widthSquare
        y: shiftH - 0.45 * widthSquare
    }
    Image {
        id: redSoldier1
        source: "qrc:/xiangqi/files/svg/soldierR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW - 0.45 * widthSquare
        y: shiftH + 3 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: redSoldier2
        source: "qrc:/xiangqi/files/svg/soldierR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 2 * widthSquare - 0.45 * widthSquare
        y: shiftH + 3 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: redSoldier3
        source: "qrc:/xiangqi/files/svg/soldierR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 4 * widthSquare - 0.45 * widthSquare
        y: shiftH + 3 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: redSoldier4
        source: "qrc:/xiangqi/files/svg/soldierR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 6 * widthSquare - 0.45 * widthSquare
        y: shiftH + 3 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: redSoldier5
        source: "qrc:/xiangqi/files/svg/soldierR.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 8 * widthSquare - 0.45 * widthSquare
        y: shiftH + 3 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackGeneral
        source: "qrc:/xiangqi/files/svg/generalW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 4 * widthSquare - 0.45 * widthSquare
        y: shiftH + 9 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackAdvisor1
        source: "qrc:/xiangqi/files/svg/advisorW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 3 * widthSquare - 0.45 * widthSquare
        y: shiftH + 9 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackAdvisor2
        source: "qrc:/xiangqi/files/svg/advisorW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 5 * widthSquare - 0.45 * widthSquare
        y: shiftH + 9 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackCannon1
        source: "qrc:/xiangqi/files/svg/cannonW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + widthSquare - 0.45 * widthSquare
        y: shiftH + 7 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackCannon2
        source: "qrc:/xiangqi/files/svg/cannonW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 7 * widthSquare - 0.45 * widthSquare
        y: shiftH + 7 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackChariot1
        source: "qrc:/xiangqi/files/svg/chariotW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW - 0.45 * widthSquare
        y: shiftH + 9 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackChariot2
        source: "qrc:/xiangqi/files/svg/chariotW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 8 * widthSquare - 0.45 * widthSquare
        y: shiftH + 9 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackElephant1
        source: "qrc:/xiangqi/files/svg/elephantW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 2 * widthSquare - 0.45 * widthSquare
        y: shiftH + 9 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackElephant2
        source: "qrc:/xiangqi/files/svg/elephantW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 6 * widthSquare - 0.45 * widthSquare
        y: shiftH + 9 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackHorse1
        source: "qrc:/xiangqi/files/svg/horseW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + widthSquare - 0.45 * widthSquare
        y: shiftH + 9 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackHorse2
        source: "qrc:/xiangqi/files/svg/horseW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 7 * widthSquare - 0.45 * widthSquare
        y: shiftH + 9 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackSoldier1
        source: "qrc:/xiangqi/files/svg/soldierW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW - 0.45 * widthSquare
        y: shiftH + 6 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackSoldier2
        source: "qrc:/xiangqi/files/svg/soldierW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 2 * widthSquare - 0.45 * widthSquare
        y: shiftH + 6 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackSoldier3
        source: "qrc:/xiangqi/files/svg/soldierW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 4 * widthSquare - 0.45 * widthSquare
        y: shiftH + 6 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackSoldier4
        source: "qrc:/xiangqi/files/svg/soldierW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 6 * widthSquare - 0.45 * widthSquare
        y: shiftH + 6 * widthSquare - 0.45 * widthSquare
    }
    Image {
        id: blackSoldier5
        source: "qrc:/xiangqi/files/svg/soldierW.svg.png"
        height: widthSquare * 0.9
        width: widthSquare * 0.9
        x: shiftW + 8 * widthSquare - 0.45 * widthSquare
        y: shiftH + 6 * widthSquare - 0.45 * widthSquare
    }
}
