#include "xiangqimodule.h"

XiangqiModule::XiangqiModule()
{
    qDebug() << "start";
    QObject::connect(this, SIGNAL(signa()), this, SLOT(resu()));
    qDebug() << signa();
}

QUrl XiangqiModule::getMainQmlUrl(QString mode)
{
    return QUrl("qrc:/xiangqi/qml/main.qml");
}

QString XiangqiModule::getName()
{
    return "Xiangqi";
}

void XiangqiModule::closeApp(){}

void XiangqiModule::setAccountName(QString login){}

bool XiangqiModule::isLocalPresent()
{
    return false;
}

bool XiangqiModule::isNetworkPresent()
{
    return false;
}

bool XiangqiModule::isOnePlayerPresent()
{
    return true;
}

bool XiangqiModule::isDualPlayerPresent()
{
    return false;
}

QString XiangqiModule::resu()
{
    qDebug() << "resu";
    return "qwe";
}
