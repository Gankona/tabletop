#include "maincontrol.h"

MainControl::MainControl()
{
    pluginList.clear();
}

void MainControl::start()
{
    loadPlugin();
    this->resize(360, 600);
    this->show();
    this->setSource(QUrl("qrc:/client/qml/main.qml"));
    this->rootContext()->setContextProperty("Qt_function", this);
    QMetaObject::invokeMethod(rootObject(), "addNewLineToListFunc", Q_ARG(QVariant, "qwes"));
//    this->openGame("Xiangqi", "");
//    this->setSource(QUrl("qrc:/client/qml/main.qml"));
//    this->openGame("Xiangqi", "");
//    this->setSource(QUrl("qrc:/client/qml/main.qml"));
//    this->openGame("Xiangqi", "");
//    this->setSource(QUrl("qrc:/client/qml/main.qml"));
//    this->openGame("Xiangqi", "");
//    this->setSource(QUrl("qrc:/client/qml/main.qml"));
}

void MainControl::quit()
{
    QGuiApplication::quit();
}

void MainControl::openGame(QString module, QString mode)
{
    for (FBasicPlugin *p : pluginList)
        if (module == p->getName()){
            this->setSource(p->getMainQmlUrl(mode));
            //this->resize(360, 600);
        }
    qDebug() << mode << module << this->errors();
}

void MainControl::getMenuList(QString var)
{
    qDebug() << "getMenuList" << var;
    if (var == "one"){
        for (FBasicPlugin *p : pluginList)
            if (p->isOnePlayerPresent())
                QMetaObject::invokeMethod(rootObject(),
                                          "addNewLineToListFunc",
                                          Q_ARG(QVariant, QVariant(p->getName())));
    }
    else if (var == "dual"){
        for (FBasicPlugin *p : pluginList)
            if (p->isDualPlayerPresent())
                QMetaObject::invokeMethod(rootObject(),
                                          "addNewLineToListFunc",
                                          Q_ARG(QVariant, QVariant(p->getName())));
    }
    else if (var == "local"){
        for (FBasicPlugin *p : pluginList)
            if (p->isLocalPresent())
                QMetaObject::invokeMethod(rootObject(),
                                          "addNewLineToListFunc",
                                          Q_ARG(QVariant, QVariant(p->getName())));
    }
    else if (var == "internet"){
        for (FBasicPlugin *p : pluginList)
            if (p->isNetworkPresent())
                QMetaObject::invokeMethod(rootObject(),
                                          "addNewLineToListFunc",
                                          Q_ARG(QVariant, QVariant(p->getName())));
    }
}

void MainControl::loadPlugin()
{
    connectPlugin("chess");
    connectPlugin("go");
    connectPlugin("shogi");
    connectPlugin("xiangqi");
}

bool MainControl::connectPlugin(QString name)
{
    QDir dir(QDir::current());
    dir.cd("modules");
    QPluginLoader loader(this);

#ifdef Q_OS_LINUX
    loader.setFileName(dir.absoluteFilePath("lib"+name+".so"));
#elif Q_OS_WIN
    loader.setFileName(dir.absoluteFilePath(name+".dll"));
#endif

    QObject *plugin = loader.instance();
    if (plugin){
        FBasicPlugin *curPlugin = qobject_cast <FBasicPlugin*> (plugin);
        pluginList.push_back(curPlugin);
        qDebug() << "module " << name << " loaded";
        return true;
    }
    qDebug() << "module " << name << " not loaded cause " << loader.errorString();
    return false;
}
