#ifndef MAINCONTROL_H
#define MAINCONTROL_H

#include "f_basicplugin.h"

#include <QDir>
#include <QDebug>
#include <QObject>
#include <QQuickView>
#include <QPluginLoader>
#include <QQmlContext>
#include <QQuickItem>
#include <QGuiApplication>

class MainControl : public QQuickView
{
    Q_OBJECT
private:
    void loadPlugin();
    inline bool connectPlugin(QString name);
    QList <FBasicPlugin*>  pluginList;

public:
    MainControl();
    void start();

signals:

public slots:
    Q_INVOKABLE void getMenuList(QString var);
    Q_INVOKABLE void quit();
    Q_INVOKABLE void openGame(QString module, QString mode);
};

#endif // MAINCONTROL_H
