import QtQuick 2.0
import QtQuick.Controls 1.4

ListView {
    property int heightButton: height/10
    property string modeName: "one"
    property string buttonText: ""
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    anchors.right: parent.right

    Connections {
        target: mainWindow
        onAddNewLineToList: {
            addButton(msg);
        }
        onSetGameMode: {
            setModeNameFunc(mode);
        }
    }

    function setModeNameFunc(mode){
        modeName = mode
    }

    function addButton(msg){
        if (msg === "")
            listChooseModel.clear()
        else
            listChooseModel.append({buttonText: msg})
    }

    delegate: Item {
        id: item
        anchors.left: parent.left
        anchors.right: parent.right
        height: heightButton

        Rectangle {
            anchors.fill: parent
            color: "orange"
            border.color: "black"

            Text {
                anchors.centerIn: parent
                text: buttonText
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Qt_function.openGame(buttonText, modeName)
                }
            }
        }
    }

    model: ListModel {
        id: listChooseModel // задаём ей id для обращения
    }
}
