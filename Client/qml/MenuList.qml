import QtQuick 2.5
import QtQuick.Controls 2.0

Rectangle {
    id: content
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    anchors.right: parent.right
    color: "red"

    signal openSetting();
    signal openAccount();
    signal openList(string msg);

    Column {
        id: contentRow
        anchors.fill: parent

        Button {
            id: oneButton
            height: parent.height/11
            width: parent.width
            anchors.margins: 10
            text: "one player"
            onClicked: openList("one")
        }

        Rectangle {
            id: dualButton
            height: parent.height/11
            width: parent.width
            color: "#4289b5"
            Text {
                text: "two player"
                anchors.centerIn: parent
            }
            MouseArea {
                anchors.fill: parent
                onClicked: openList("dual")
            }
        }

        Rectangle {
            id: lanButton
            height: parent.height/11
            width: parent.width
            color: "khaki"
            gradient: Gradient {
                GradientStop {
                    position: 0.00;
                    color: "#f0e68c";
                }
                GradientStop {
                    position: 1.00;
                    color: "#ffffff";
                }
            }
            Text {
                text: "local game"
                anchors.centerIn: parent
            }
            MouseArea {
                anchors.fill: parent
                onClicked: openList("local")
            }
        }

        Rectangle {
            id: internetButton
            height: parent.height/11
            width: parent.width
            color: "khaki"
            gradient: Gradient {
                GradientStop {
                    position: 0.00;
                    color: "#f0e68c";
                }
                GradientStop {
                    position: 1.00;
                    color: "#992828";
                }
            }
            Text {
                text: "internet game"
                anchors.centerIn: parent
            }
            MouseArea {
                anchors.fill: parent
                onClicked: openList("internet")
            }
        }

        Rectangle {
            id: settingButton
            height: parent.height/11
            width: parent.width
            color: "khaki"
            Text {
                text: "settings"
                anchors.centerIn: parent
            }
            MouseArea {
                anchors.fill: parent
                onClicked: openSetting()
            }
        }
    }

    Rectangle {
        id: accountButton
        height: parent.height/11
        width: parent.width
        anchors.bottom: editButton.top
        color: "#7934aa"
        Text {
            id: accountText
            anchors.centerIn: parent
            text: "login"
        }
        MouseArea {
            anchors.fill: parent
            onClicked: openAccount()
        }
    }
    Rectangle {
        id: editButton
        height: parent.height/11
        width: parent.width
        anchors.bottom: parent.bottom
        color: "darkgreen"
        Text {
            anchors.centerIn: parent
            text: "Выход"
        }
        MouseArea {
            anchors.fill: parent
            onClicked: Qt_function.quit()
        }
    }
}
