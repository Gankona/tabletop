import QtQuick 2.5
import QtQuick.Controls 1.4
import "qrc:/client/qml"

Item {
    id: mainWindow
    anchors.fill: parent

    property bool isMenu: false

    signal addNewLineToList(string msg);
    signal setGameMode(string mode);

    function addNewLineToListFunc(msg){
        console.log("-------------------------------------------- addNewFunc", msg)
        addNewLineToList(msg)
    }

    Account {
        id: account
        visible: false
        height: menu.visible ? parent.height - menu.height : parent.height
    }

    ListChoose {
        id: listChoose
        visible: false
        height: menu.visible ? parent.height - menu.height : parent.height
    }

    MenuList {
        id: menuList
        visible: true
        height: menu.visible ? parent.height - menu.height : parent.height
        onOpenAccount: {
            account.visible = true
            menuList.visible = false
            menu.visible = true
            menuTitle.text = "one Player"
        }
        onOpenSetting: {
            menuList.visible = false
            menu.visible = true
            setting.visible = true
            menuTitle.text = "Settings"
        }
        onOpenList: {
            setGameMode(msg)
            listChoose.visible = true
            menuList.visible = false
            menu.visible = true
            addNewLineToList("")
            Qt_function.getMenuList(msg)
        }
    }

    Settings {
        id: setting
        visible: false
        color: "green"
        height: menu.visible ? parent.height - menu.height : parent.height
    }

    Rectangle {
        id: menu
        visible: false
        height: parent.height / 11
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        color: "blue"

        Text {
            id: menuTitle
            anchors.centerIn: parent
        }

        Rectangle {
            id: backButton
            height: parent.height
            width: parent.height
            anchors.top: parent.top
            anchors.left: parent.left
            color: "blue"
            Image {
                anchors.fill: parent
                source: "qrc:/client/images/backbutton.png"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    account.visible = false
                    listChoose.visible = false
                    menuList.visible = true
                    setting.visible = false
                    menu.visible = false
                }
            }
        }
    }
}
