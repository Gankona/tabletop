#include "maincontrol.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    MainControl *control = new MainControl;
    control->start();

    return app.exec();
}
