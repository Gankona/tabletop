QT += qml quick core

CONFIG += c++14

DESTDIR = /opt/TableTop

SOURCES += main.cpp \
    maincontrol.cpp

RESOURCES += qml.qrc

OTHER_FILES += $$PWD/qml/*.qml

DISTFILES += \
    Settings.qml \
    qml/Account.qml \
    qml/ListChoose.qml \
    qml/MenuList.qml

HEADERS += \
    f_basicplugin.h \
    maincontrol.h
