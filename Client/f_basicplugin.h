#ifndef FBASICPLUGIN
#define FBASICPLUGIN

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QtPlugin>
#include <QtCore/QPluginLoader>
#include <QtCore/QUrl>

class FBasicPlugin : public QObject
{
public:
    virtual ~FBasicPlugin(){}
    virtual void closeApp() = 0;
    virtual void setAccountName(QString login) = 0;
    virtual bool isLocalPresent() = 0;
    virtual bool isNetworkPresent() = 0;
    virtual bool isOnePlayerPresent() = 0;
    virtual bool isDualPlayerPresent() = 0;
    virtual QUrl getMainQmlUrl(QString mode) = 0;
    virtual QString getName() = 0;

signals:
    virtual void signalBackToMenu() = 0;
    virtual void signalCloseApp() = 0;
};

#define FBasicPlugin_iid "org.gankona.TableTop.FBasicPlugin"
Q_DECLARE_INTERFACE(FBasicPlugin, FBasicPlugin_iid)

#endif
